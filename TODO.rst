
# labpy to-do list

peak width sorting 


signal gen sim
signal sim:
- droplet sim

overlay plots
statistic view


implementing oszi mode (monitor, threshold)

combining time data with measurements ...

optimise user interactions
name save std data canonical filename
better serial interface handling (define interface to monitor)

voltage calibration (display)

stat view - histogram view

recording indicator

sort setting feedback

openCV video stream

redesign oszi widget (group widget)

pump

pump slider connect to events

device control process language

pump algorithm 

pumpprofile class

process language

droplet calculator:
basic: radius, volume, surface
concentration: num of water, num of molecules at given concentration, diffusion times,
viscosity ?

droplet analyzer:
counting droplets and size distribution histogram
std. dev. of sizes ....

visible when added -> view / tools menu

LabTimer Widget

Wave generator 
- QT phonon based

labworkbench:
New Project dir ...

python shell calculator
voltage devider calculator


LARA Process editor


Labview
max/min
peak width
 

gaussian_kde kernel d? e? (s. numpy)

2D histogram (s. plotly)
Voltage vs size / scatter plot with gathing

