#! /usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: labPy

*labpy_hello_world_app.py: minimalistic labPy app*

:details: : some improvements/features over plain labpy_hello_world_app.

:file:    labpy_hello_world_app.py

:author:  mark doerr <mark.doerr@uni.greifswald.de> : 
          
:date: (creation)          20181104
:date: (last modification) 20181104
.. note:: some remarks
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "v0.0.3"


import sys

import argparse
import logging

import labpy.labpyworkbench as lpwb
import labpy.widgets.display_alphanumeric_widget as danw
import labpy.widgets.timer_widget as timew
import labpy.widgets.temperature_widget as tempw

if __name__ == "__main__":
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    
    parser = argparse.ArgumentParser(description="simple control, plot, evaluation framework for microfluidic experiments")

    # just some examples for command line parsing
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
        
    parsed_args = parser.parse_args()
    
    appname = "labpy_thermometer"
    appauthors = "mark doerr (mark.doerr@uni-greifswald.de)"
    applicense = "GLP-3"
    applink = "< a href=\"http://gitlab.com/larasuite/labpy\"> http://gitlab.com/larasuite/labpy </a>"
    description = ("<p><b>{appname}</b> is a very simple labpy temperature app <br /> "
                   "demonstrating the different labPy temperature displays. </p>"
                  "<p><b>Authors:</b> {authors} </p>"
                  "<p>No Warrenty. License: {license} </p>"
                  "<p><b>WebLink:</b> {link} </p>").format(appname=appname, authors=appauthors, license=applicense, link=applink)
    
    labPy_app = lpwb.LP_Application(sys.argv, appname=appname, description=description)
    
    timew.TimerWidget(LP_application=labPy_app)
    
    # temperature toolbar widget
    temp_tb_widget = tempw.TemperatureWidget(LP_application=labPy_app)
    temp_tb_widget.setTemperature(42.0)
    
    # temp display widget
    temp_tb_widget = tempw.TemperatureWidget(LP_application=labPy_app, pos=lpwb.WidgetPosition.CENTER)
    
    temp_tb_widget.setFontSize(66)
    temp_tb_widget.setTemperature(42.0)
    
    
    labPy_app.run()
    
