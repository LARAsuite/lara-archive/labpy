#! /usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: labPy

*labpy_hello_world_app.py: minimalistic labPy app*

:details: : bare minimum labpy application

:file:    labpy_hello_world_app.py

:author:  mark doerr <mark.doerr@uni.greifswald.de> : 
          
:date: (creation)          20181104
:date: (last modification) 20181104
.. note:: some remarks
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "v0.0.3"

import sys

import labpy.labpyworkbench as lpwb
import labpy.widgets.display_alphanumeric_widget as danw

if __name__ == "__main__":
    
    # generate labPy application 
    labPy_app = lpwb.LP_Application(sys.argv, appname="labpy_hello_world")
    
    # create a TextDisplay
    hello_display = danw.TextDisplayWidget(LP_application=labPy_app, name="Text display")

    # set the text in the Text Display
    hello_display.setText("Hello pyLab world !")
                                    
    # run the application
    labPy_app.run()
    
