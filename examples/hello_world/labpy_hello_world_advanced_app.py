#! /usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: labPy

*labpy_hello_world_app.py: minimalistic labPy app*

:details: : some improvements/features over plain labpy_hello_world_app.

:file:    labpy_hello_world_app.py

:author:  mark doerr <mark.doerr@uni.greifswald.de> : 
          
:date: (creation)          20181104
:date: (last modification) 20181104
.. note:: some remarks
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  For further Information see COPYING file that comes with this distribution.
________________________________________________________________________

"""

__version__ = "v0.0.3"


import sys

import argparse
import logging

import labpy.labpyworkbench as lpwb
import labpy.widgets.display_alphanumeric_widget as danw

if __name__ == "__main__":
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    
    parser = argparse.ArgumentParser(description="simple control, plot, evaluation framework for microfluidic experiments")

    # just some examples for command line parsing
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
        
    parsed_args = parser.parse_args()
    
    appname = "labpy_hello_world"
    appauthors = "mark doerr (mark.doerr@uni-greifswald.de)"
    applicense = "GLP-3"
    applink = "< a href=\"http://gitlab.com/larasuite/labpy\"> http://gitlab.com/larasuite/labpy </a>"
    description = ("<p><b>{appname}</b> is a very simple labpy hello world app (advanced). </p>"
                  "<p><b>Authors:</b> {authors} </p>"
                  "<p>No Warrenty. License: {license} </p>"
                  "<p><b>WebLink:</b> {link} </p>").format(appname=appname, authors=appauthors, license=applicense, link=applink)
    
    labPy_app = lpwb.LP_Application(sys.argv, appname=appname, description=description, 
                                    toolbar=False, bottomtab=False)
    
    hello_display = danw.TextDisplayWidget(LP_application=labPy_app, name="Text display", 
                                    pos=lpwb.WidgetPosition.CENTER)

    # one can add all types of stylesheets (hello_display is a normal QT5 widget)
    hello_display.setStyleSheet( """QPlainTextEdit {background-color: #333;
                                    color: #00FF00;
                                    /*text-decoration: underline*/;
                                    font: 42pt;}""")

    hello_display.setText("Hello pyLab world - display with style ;) !")
    
    labPy_app.run()
    
